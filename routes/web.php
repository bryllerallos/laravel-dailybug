<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dailybuglanding');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//for all bugs
Route::get('/allbugs', 'BugController@index');

Route::get('/indivbug/{id}', 'BugController@showIndivBug');

//user routes
//to show add bug form
Route::get('/addbug', 'BugController@create');

//to save

Route::post('/addbug', 'BugController@store');

//to show user bugs

Route::get('/mybugs', 'BugController@indivBugs');

//to delete bug

Route::delete('/deletebug/{id}', 'BugController@destroy');

//to go to the edit form

Route::get('/editbug/{id}', 'BugController@edit');

//to save edited bug

Route::patch('/editbug/{id}', 'BugController@update');

//admin routes
//to go th solve form

Route::get('/solve/{id}', 'BugController@showSolve');

//to save

Route::post('/solve', 'BugController@saveSolution');

//to delete solutions

Route::delete('/deleteSolutions/{id}', 'SolutionController@destroy');

//to accept solution

Route::patch('/accept/{id}', 'BugController@accept');