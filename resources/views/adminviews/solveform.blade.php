@extends('layouts.app')
@section('content')


<h1 class="text-center py-5">Bug Fix</h1>
<div class="row">
	<div class="col-lg-4 offset-lg-1 bg-info">
		<h3 class="text-dark">Bug Title:{{ $bug->title }}</h3>
		<p>{{ $bug->body }}</p>
		<h6>Category: {{ $bug->category->name }}</h6>
		<h6>Status: {{ $bug->status->name }}</h6>
		<h6>Requested by: {{ $bug->user->name }}</h6>
	</div>
	<div class="col-lg-4 offset-lg-2">
		<h3>Solution:</h3>
		<form action="/solve" method="POST">
			@csrf
			<div class="form-group">
				<label for="title">Title:</label>
				<input type="text" name="title" class="form-control">
			</div>
			<div class="form-group">
				<label for="body">Body:</label>
				<input type="text" name="body" class="form-control">
			</div>
			<div class="text-center">
			<input type="hidden" name="bug_id" value="{{$bug->id}}">
			<button class="btn btn-warning">Solve</button>
			</div>

		</form>
	</div>
</div>

@endsection