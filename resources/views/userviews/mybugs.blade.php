@extends('layouts.app')
@section('content')



<h1 class="text-center py-5">My Bugs</h1>

<div class="container">
<div class="row">
@foreach($bugs as $indiv_bug)
<div class="col-lg-3 my-2">
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">{{$indiv_bug->title}}</h4>
			<p class="card-text">{{$indiv_bug->body }}</p>
			<p class="card-text">{{$indiv_bug->category->name}}</p>
			<p class="card-text">{{$indiv_bug->status->name }}</p>
		</div>
		<div class="card-footer d-flex">
			<form action="/deletebug/{{$indiv_bug->id}}" method="POST">
				@csrf
				@method('DELETE')
				<button class="btn btn-danger mr-2 py-4" type="submit">Delete</button>
			</form>
			<a href="/editbug/{{$indiv_bug->id}}" class="btn btn-success mr-2">Edit</a>
			<a href="/indivbug/{{ $indiv_bug->id }}" class="btn btn-secondary mr-2">View Details</a>
		</div>
	</div>


</div>
@endforeach
</div>
</div>

	


@endsection