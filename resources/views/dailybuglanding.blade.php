<!DOCTYPE html>
<html>
<head>
	<title>Daily Bug</title>

	  <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

	<div class="d-flex vh-100">
		<div class="vh-100 w-50 align-items-center justify-content-end d-flex" 
			 style="background-color: #A8E0FF; color: #0A0908; ">
			 	<h1>DAILY</h1>
			 </div>
		<div class="d-flex vh-100 w-50 align-items-center"
			style="background-color: #0A0908; color:#A8E0FF;">
				<h1>BUG</h1>
			</div>
		
	</div>

</body>
</html>